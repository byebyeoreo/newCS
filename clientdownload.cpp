#include "clientdownload.h"
#include "client.h"


void DownLoadInit(int Telnet_port)
{
	if (TRUE == OspInit(TRUE, Telnet_port))
		//de_bug
		//printf("Init success!\n");//OSP初始化
		OspCreateTcpNode(0, FT_CLIENT_PORT);	//创建监听节点
	g_FTClientApp.CreateApp("FTClientApp", FT_CLIENT_APP_ID, FT_CLIENT_APP_PRIO, FT_APP_QUE_SIZE);
}
//发送链接请求，参数CfgPath为配置文件路径
void CreateConn(char* CfgPath)
{
	while (1)
	{
		setbuf(stdin, NULL);// 清空标准输入
		printf("\nInput character 'c' to connect the server\n");

		if (getchar() == 'c')
		{
			OspPost(MAKEIID(FT_CLIENT_APP_ID, CInstance::PENDING), FT_EVENT_C2S, CfgPath, strlen(CfgPath)); //连接服务器
			break;
		}
	}
   
}

//从终端读取你所希望获得的文件夹列表的路径，回车默认返回存储根目录文件列表
void GetFileNameList(char* FileList)
{
	::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_INQUIRE_LIST, FileList, strlen(FileList));

}

//发送下载命令，需要从终端读入要下载文件的相对路径（可以根据GetFileNameList()函数获得路径），
//
void SendDownldCmd(char* ReqFileName)
{
	if (strlen(ReqFileName) == 1)
	{
		printf("Your input is empty\n");
		exit(1);
	}
	::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_REQ_URL, ReqFileName, strlen(ReqFileName));
}

void quit()
{
	OspQuit();								// 退出Osp
}