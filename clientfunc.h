#ifndef _CLIENTFUNC_H_
#define _CLIENTFUNC_H_
#include "common.h"
#include "md5.h"

s64 IsFileExist(s8* filepath);
BOOL32 CheckDirExist(const s8* dirpath);
s32 GetKeyValue(s8* KeyName, s8* ValueString, u8* Path);
BOOL32 GetFileNameFromUrl(const s8* URL, s8*fullname);
BOOL32 GetFullPath(const s8* storagepath, const s8* filename, s8* filepath);
BOOL32 FileMd5Check(const s8* storagepath, const u8* md5, const s8* filename, s64 * OffSet);
#endif // !_CLIENT_H_
