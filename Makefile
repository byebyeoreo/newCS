CC := g++  
CFLAGS := -w -D _LINUX_ -DMG_DISABLE_DAV_AUTH -DMG_ENABLE_FAKE_DAVLOCK 
OBJS = main.o client.o
LDFLAGS = -static -L/root/client_new -losp -lcurl -lpthread -lrt -ldl
all:$(OBJS)
$(OBJS):%.o:%.cpp
	$(CC) -c $(CFLAGS) $< -o $@
client:$(OBJS)
	$(CC) -o client $(OBJS) $(CFLAGS) $(LDFLAGS)
clean:  
	rm -rf client *.o