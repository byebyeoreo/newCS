#pragma once
#ifndef FT_CLIENT_H_
#define FT_CLIENT_H_

#include "common.h"
#include <curl/curl.h>

extern u8 CfgPath[FILEPATH_ABS_LEN];  //配置文件位置

//extern s32 FT_CLIENT_PORT;			//客户端端口
//extern s32 FT_CLIENT_APP_ID;		//客户端APP ID

extern s8 Server_Ip[15];			//OSP服务器 IP
extern s32 FT_SERVER_PORT;			//OSP 服务器端口
extern s32 OSP_SERVER_APP_ID;		//OSP服务器APP ID

#define MAXDOWNLOADPTHREAD 6		//定义最大下载线程个数


//读取配置文件函数
//若 ValueString 为NULL，返回int类型；若传入的ValueString指针非空，则带出char类型，返回值无实际意义
s32 GetKeyValue(s8* KeyName, s8* ValueString, u8* Path);

//定义FTcInstance类,继承自CInstance
class FTcInstance : public CInstance
{
public:
	FTcInstance();
	~FTcInstance();

	//消息处理函数
	void InstanceEntry(CMessage *const pMsg);
	void DaemonInstanceEntry(CMessage *const pMsg, CApp *pcApp);


private:
	void ClientInitRequest(CMessage *const pMsg);			//文件传输客户端初始化
	void ClientProcessReply(CMessage *const pMsg);			//客户端处理服务器发来的请求回复
	void ClientQueryFile(CMessage *const pMsg);				//查询服务器文件命令
	void ClientReqURL(CMessage *const pMsg);				//请求下载文件
	void ClientDownload(CMessage *const pMsg);				//下载文件
	void ClientDisconn(CMessage *const pMsg);				//退出
	
	void ClientTest(CMessage *const pMsg);				//客户端调试

private:
	void DealConnReply(CMessage *const pMsg);
	void DealQueryFileReply(CMessage *const pMsg);
	void DealReqUrlRply(CMessage *const pMsg);
	s32 CurlDownload(const s8* url,const u8* md5);
private:
	u32 m_dwServerNode;									//服务器Node号
	u32 m_dwSevInstancId;								//服务器INS的ID
	BOOL32 m_bConn;										//客户端状态
	u32 m_dwDownloadPthreadNumb;						//下载线程个数
private:
	s8 m_achLclStorgePath[FILEPATH_ABS_LEN];			//下载文件存储路径
};

typedef zTemplate<FTcInstance, FT_CLIENT_INS_NUM, CAppNoData, 20> FTClientApp;
FTClientApp g_FTClientApp;
#endif