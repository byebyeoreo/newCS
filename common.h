#pragma once
#ifndef _COMMON_H_
#define _COMMON_H_

#pragma once
#include <string>
#include <string.h>
#include<stdio.h>
#include "kdvtype.h"
#include "osp.h"





#define DisCon_Detection_Timeout	3                            //断链检测超时时间
#define DisConn_Detction_Num		3                            //断链检测次数
//定义客户端相关常量
#define FT_CLIENT_PORT              8481                         //客户端端口号                       
#define FT_CLIENT_APP_ID            3                            //客户端APP号
#define FT_CLIENT_APP_PRIO          100                          //客户端APP优先级                               
#define FT_CLIENT_INS_NUM           1                            //instance 数 
//设置服务器参数

//#define FT_SERVER_APP_ID				1                            //服务器APP号 
#define FT_SERVER_APP_PRIO				100                          //服务器APP优先级                    
#define FT_SERVER_MAX_INS				50                           //服务器APP最大实例数

#define FT_APP_QUE_SIZE				1000							//最大等待消息数
//定义文件相关信息

#define FILENAME_MAX_LEN			256								//定义文件名称最大长度
#define FILEPATH_ABS_LEN			1024							//定义文件完整路径最大长度
//const   s8 Server_Ip[] = "123.207.39.45";								//服务器IP
//#define LclStorgePath		"/home/downloads/"							//定义本地存储路径
//#define FT_SERVER_PORT				7990                         //服务器端口号 
																		//定义传输字符串流的最大长度
#define FILE_STREAM_MAX_LEN			4096							//传输字符串的最大长度

																		//定义Instance状态
#define DIS_CONNECTED_STATE           (u32)0                         //断开状态
#define CONNECTED_STATE               (u32)1                         //连接状态(也是未传输文件状态)
#define TRANSMITTING_STATE			  (u32)2						   //传输文件状态

#define RUNNING_STATE				(s16)1 							//定义INS运行状态
#define IDLE_STATE					(s16)0							//定义INS空闲状态

																		//定义客户端事件类型
#define FT_EVENT_C2S                (OSP_USEREVENT_BASE+1)				 //客户端请求连接服务器
#define FT_EVENT_C_PRCSS_RPLY_C2S   (OSP_USEREVENT_BASE+2)				 //客户端处理服务器关于请求连接的回复事件
#define FT_EVENT_C_INQUIRE_LIST     (OSP_USEREVENT_BASE+3)				 //客户端查询服务器文件事件
#define FT_EVENT_C_INQUIRE_ACK_RCV  (OSP_USEREVENT_BASE+4)				//客户端处理服务器列出可下载文件事件
#define FT_EVENT_C_REQ_URL		(OSP_USEREVENT_BASE+5)				//客户端请求文件URL事件
#define FT_EVENT_C_PRCSS_RPLY_URL	(OSP_USEREVENT_BASE+6)			//客户端处理服务器关于下载文件的回复事件
#define FT_EVENT_C_DOWNLOAD			(OSP_USEREVENT_BASE+7)				 //客户端续传文件事件
#define FT_EVENT_C_TEST				(OSP_USEREVENT_BASE+14)				//客户端测试
																		//定义服务端事件类型
#define FT_EVENT_S_PRCSS_REQ			(OSP_USEREVENT_BASE+8)			//服务器处理来自客户端的请求
																		//#define FT_EVENT_S_SET_HEARTBEAT		(OSP_USEREVENT_BASE+9)			//服务器INS接受连接，设置心跳函数
#define FT_EVENT_S_FILE_LIST		(OSP_USEREVENT_BASE+10)				//服务器列出可供下载文件事件
#define FT_EVENT_S_PRCSS_REQ_DOWNLOAD (OSP_USEREVENT_BASE+11)			//服务器处理请求下载事件
																		//#define FT_EVENT_S_RETRY			(OSP_USEREVENT_BASE+12)				//服务器处理续传文件事件
#define FT_EVENT_S_DOWNLOADING		(OSP_USEREVENT_BASE+13)				//服务器处理列出正在下载文件客户端事件


#define StructMassageSize				4000

typedef struct tagMsg
{
	BOOL32 m_bFlag;		//用于判断客户端的请求服务器是否满足
	s8	m_achMsgHeader;	//消息体信息摘要
	s8  m_achMsgBody[StructMassageSize];		//服务器回复的消息（包括附加说明，请求文件URL等）
}TMsg;
#endif
