#include"client.h"
#include "clientdowmload.h"
#define CfgPath "/root/client07/client.cfg"
//函数声明
void func(); //用户操作函数
int main(int argc, char* argv[])
{
	Init(2501);
	//发起连接
	CreateConn(CfgPath);
	sleep(1);
	func();
	return 0;
}


//程序运行函数，主要包含读取输入的查询文件名和下载文件名并发送相应命令
void func()
{
	u16 i;
	for(;;)
	{
		sleep(1);	//并非必要，因为客户端是异步发送消息，所以此举是为了等待终端显示服务器回复消息
		printf("------------------------------------------------\n");
		//printf("客户端提供以下4种功能：\n 1. 查询服务器文件列表 | 2. 下载文件 | 3.退出OSP 	输入编号以进入相应功能：\n");
		printf("The client provide 3 function:\n [1]: query the file list \n [2]: download \n [3]:	quit OSP\nEnter your choice:\n");
		while(1)
		{	
			setbuf(stdin,NULL);// ?????????
			if(!scanf("%d",&i))
				printf("Incorrect input parameter\n");//getchar();
			else
				break;
		}
			if(i == 1)
			{
				printf("Enter the file path that you want to get,(if emtpy, Server will retrun a list of files in the storage root directory)\n ");
				printf("If you don't know the file path,press Enter \n");
				s8 filepath[FILEPATH_ABS_LEN];
				memset(filepath, 0, FILEPATH_ABS_LEN);
				//printf("===================================================================\n");
				setbuf(stdin, NULL);// 清空标准输入
				fgets(filepath, FILEPATH_ABS_LEN,stdin);
				//de_bug
				//printf("filepath is %s\n",filepath);

				if (strlen(filepath) == 1)
				{
					//无参查询
					GetFileNameList(NULL);
				}
				else
				{
					GetFileNameList(filepath);
				}
			}
			else if(i == 2)
			{
				s8 ReqFileName[FILEPATH_ABS_LEN];
				setbuf(stdin, NULL);// 清空标准输入
				fgets(ReqFileName, FILEPATH_ABS_LEN,stdin);
				SendDownldCmd(ReqFileName);

			}
			else if(i == 3)
			{
				printf("quiting...\n");
				quit();
				sleep(1);
				printf("quit success!\n");
					
				break;
			}
			else
			{
				printf("Incorrect input parameter(enter 1,2,3)\n");
			}

	}
}
