#ifndef _CLIENTDOWNLOAD_H_
#define _CLIENTDOWNLOAD_H_
#pragma once
#ifdef PLATFORMSDK_EXPORT
#define PLATFORMSDK __declspec(dllexport)
#else
#define PLATFORMSDK __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C" {
#endif
	void DownLoadInit(int Telnet_port);
	void CreateConn(char* CfgPath);
	void GetFileNameList(char*FileList);
	void SendDownldCmd(char*ReqFileName);
	void quit();


#ifdef __cplusplus
}
#endif

#endif





