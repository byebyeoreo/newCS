#include "clientfunc.h"


/*====================================================================
模块名：	查询文件是否存在
返回值：	返回0代表此文件不存在或者偏移量为0，若存在部分，则返回实际的偏移量
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/
s64 IsFileExist(s8* filepath)
{
	FILE* filefd = NULL;
	if ((filefd = fopen(filepath, "r")) == NULL)
	{
		printf("本地文件不存在(local file doesnot exit)!\n");
		return 0;
	}
	s32 fseek_ret = fseeko(filefd, 0, SEEK_END);
	if (fseek_ret == -1)
	{
		perror("fseek error!\n");
	}
	off_t FileOffset = ftello(filefd);  //获取文件偏移量当前
	fclose(filefd);

	return FileOffset;
}

//函数声明：检查本地下载文件夹是否存在，不存在，则创建
BOOL32 CheckDirExist(const s8* dirpath)
{
	if (access(dirpath, F_OK) == 0)
		return TRUE;
	else
	{
		umask(0);
		if (mkdir(dirpath, S_IRWXU | S_IRWXG | S_IRWXO) == 0)
			return TRUE;
		else
		{
			printf("Fail to create directory, downloading terminated\n");
			return FALSE;
		}
	}
}

//读取配置信息函数 若 ValueString 为NULL，返回s32类型；若传入的ValueString指针非空，则带出s8类型，返回值无实际意义
s32 GetKeyValue(s8* KeyName, s8* ValueString, u8* Path)
{
	return 0;

}


//从URL中获取要下载文件的本地全路径

BOOL32 GetFileNameFromUrl(const s8* URL, s8*filename)
{
	//从URL中获取文件名，用于查询本地是否已经下载部分文件
	if (NULL == URL)
		return FALSE;
	s32 i = strlen(URL) - 1;
	while (*(URL + i) != '/')
	{
		--i;
	}
	++i;
	s32 j = 0;
	//从URL中获取下载文件的名称（也可从本地请求获取）
	while (*(URL + i) != '\0')
	{
		*(filename + j++) = *(URL + i++);
	}
	return TRUE;
}
BOOL32 GetFullPath(const s8* storagepath, const s8* filename, s8* filepath)
{
	if (NULL == storagepath || NULL == filename)
		return FALSE;
	//printf("Insider::storagepath is %s\n filename is %s\n",storagepath,filename);
	s32 len = strlen(storagepath) - 1;

	while ((*(storagepath + len) == ' '))
	{
		len--;
	}

	s32 i;
	for (size_t i = 0; i <= len; i++)
	{
		*(filepath + i) = *(storagepath + i);
	}
	if (*(storagepath + len) != '/')
		strcat(filepath, "/");
	strcat(filepath, filename);
	//printf("Insider::filepath is %s\n",filepath);
	return TRUE;

}
//校验文件的md值并保存md5值到本地
BOOL32 FileMd5Check(const s8* storagepath, const u8* md5, const s8* filename, s64* OffSet)
{
	//检查路径是否存在
	if (0 == CheckDirExist(storagepath))
	{
		printf("Check storagepath failed \n");
		return FALSE;
	}
	s8 filepath[FILEPATH_ABS_LEN];
	if (0 == GetFullPath(storagepath, filename, filepath))
	{
		printf("获取文件绝对路径失败(ERR: Get no file abs path)\n");
	}
	printf("-----------------------------------------\n");
	s8 md5filepath[FILEPATH_ABS_LEN + 4];
	memset(md5filepath, 0, sizeof(md5filepath));
	strcpy(md5filepath, filepath);
	strcat(md5filepath, ".md5");
	//printf("filepath is %s \nmd5filepath is %s \n",filepath,md5filepath);
	s32 bRet = IsFileExist(md5filepath);
	if (bRet == 0)
	{
		//若本地不存在该文件，创建对应文件的md5文件；若存在,则清空该文件重写
		FILE* fp = NULL;
		if ((fp = fopen(md5filepath, "w+")) == NULL)
		{
			printf("创建md5文件失败!\n");
			return FALSE;
		}
		s32 bRetWrite;
		bRetWrite = fwrite(md5, sizeof(u8), 33, fp);
		printf("bRet is 0,bRetWrite = %d\n", bRetWrite);
		if (!(bRetWrite == 33 || bRetWrite == 32))
		{
			printf("md5值写入文件错误\n");
			fclose(fp);
			return FALSE;
		}
		fclose(fp);
		printf("~~~~~~~~~~~~~\n");
		//创建或者清空要下载的文件
		FILE* fp1 = NULL;
		fp1 = fopen(filepath, "w");
		{
			if (NULL == fp1)
			{
				printf("删除已下载部分文件或者创建文件失败()fail to del part file or fail to create file \n");
				return FALSE;
			}
		}
		fclose(fp1);
		*OffSet = 0l;
		return TRUE;
	}
	//存在md5校验文件
	else
	{
		//判断md5值是否一致
		u8 md5buf[33];
		FILE* fp = NULL;
		if ((fp = fopen(md5filepath, "r")) == NULL)
		{
			printf("打开md5文件失败\n");
			return FALSE;
		}
		s32 nReadNumb = fread(md5buf, sizeof(u8), 33, fp);
		if (!(nReadNumb == 33 || nReadNumb == 32))
		{
			printf("md5文件读取错误\n");
			fclose(fp);
			return FALSE;
		}
		fclose(fp);
		//如果md5值一致，返回已下载文件偏移量
		if (0 == memcmp(md5, md5buf, 32))
		{
			*OffSet = IsFileExist(filepath);
			//printf("*OffSet = %lld\n",*OffSet);

		}
		//md5值不一致，更新md5文件，并将已下载的部分清空
		else
		{
			//将新的md5写入md5文件
			fp = NULL;
			if ((fp = fopen(md5filepath, "w+")) == NULL)
			{
				printf("打开要更新的md5文件失败!\n");
				return FALSE;
			}
			s32 bRetWrite;
			bRetWrite = fwrite(md5, sizeof(u8), 33, fp);
			if (!(bRetWrite == 33 || bRetWrite == 32))
			{
				printf("md5值写入文件错误\n");
				fclose(fp);
				return FALSE;
			}
			//printf("bRetWrite = %d\n",bRetWrite);
			//清空要下载的文件
			FILE* fp1 = NULL;
			fp1 = fopen(filepath, "w");
			{
				if (NULL == fp1)
				{
					printf("删除已下载部分文件失败\n");
					return FALSE;
				}
			}
			fclose(fp1);
			*OffSet = 0;
		}

		return TRUE;
	}


}
