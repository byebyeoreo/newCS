#include "client.h"
#include "clientfunc.h"
//全局变量声明
 s8 Server_Ip[15];			//OSP服务器 IP
 s32 FT_SERVER_PORT;			//OSP 服务器端口
 s32 OSP_SERVER_APP_ID;		//OSP服务器APP ID
//函数声明
s32 CurlDownload(const s8* url, const s8* filepath, s64 OffSet);

/*====================================================================
模块名：	构造函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
FTcInstance::FTcInstance() {
	m_dwServerNode = 0;							 //服务器Node号
	m_dwSevInstancId = 0;						//服务器INS的ID
	m_bConn = FALSE;						//客户端状态
	memset(m_achLclStorgePath, 0, sizeof(m_achLclStorgePath));
	m_dwDownloadPthreadNumb = 0;
	//memset(m_filename,0,sizeof(m_filename));	//初始化
}


/*====================================================================
模块名：	析构函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
FTcInstance::~FTcInstance() {

}

//守护实例入口
void FTcInstance::DaemonInstanceEntry(CMessage *const pMsg, CApp *pcApp)
{
	if(pMsg->content != NULL)
		printf("DAEMON %s\n",pMsg->content);
	if( NULL == pMsg || NULL == pcApp )
    {
        return;
    }

    u16 wEvent = pMsg->event;                        //接收事件类型

    switch( wEvent )                                 //判断事件类型，进行相应处理
    {
    default:
        break;
    }	
}

//普通实例入口
void FTcInstance::InstanceEntry(CMessage *const pMsg)
{
	//if(pMsg->content != NULL)
		//printf("%s\n",pMsg->content);
	u16 wCurEvent = pMsg->event;
	switch (wCurEvent)
	{
	case FT_EVENT_C2S:				//客户端请求连接
		ClientInitRequest(pMsg);
		NextState(RUNNING_STATE);
		break;
	case FT_EVENT_C_PRCSS_RPLY_C2S:				//客户端处理服务器关于请求连接的回复
	case FT_EVENT_C_PRCSS_RPLY_URL:				//客户端处理请求URL回复
	case FT_EVENT_C_INQUIRE_ACK_RCV:				//客户端处理服务器关于查询的回复
		ClientProcessReply(pMsg);	
		break;
	case FT_EVENT_C_INQUIRE_LIST:				//客户端查询可下载文件
		ClientQueryFile(pMsg);
		break;
	case FT_EVENT_C_REQ_URL:				//客户端请求文件URL
		ClientReqURL(pMsg);
		break;

	case FT_EVENT_C_DOWNLOAD:
		ClientDownload(pMsg);
		break;
	case FT_EVENT_C_TEST:
		ClientTest(pMsg);
		break;
	case OSP_DISCONNECT:				//	osp	断开链接
		ClientDisconn(pMsg);
		NextState(IDLE_STATE);
	default:
		OspPrintf(TRUE, FALSE, "\n错误, 非法事件类型！\n");
		break;
	}
}

// de_bug 函数
void FTcInstance::ClientTest(CMessage *const pMsg)
{
	printf("NOTE:	This is a debug massage\n");
	printf("pMsg->content is %s \n",pMsg->content);
}

//客户端初始化以及请求连接函数

void FTcInstance::ClientInitRequest(CMessage *const pMsg)
{
	//读取配置文件
	memcpy(CfgPath,pMsg->content,sizeof(CfgPath));

	//s32 ret = GetKeyValue("HTTP_IP", http_ip, CfgPath); //载入http ip
	//s32 http_port = GetKeyValue("HTTP_PORT", NULL, CfgPath); //载入http port

	s32 OSP_SERVER_APP_ID = GetKeyValue("OSP_SERVER_APP_ID", NULL, CfgPath);//载入OSP服务器APP ID
	s32 FT_SERVER_PORT = GetKeyValue("FT_SERVER_PORT", NULL, CfgPath);//载入OSP 服务器端口
	s32 ret = GetKeyValue("OSP_SERVER_IP", Server_Ip, CfgPath);	//载入OSP服务器IP

	//s32 FT_CLIENT_APP_ID = GetKeyValue("FT_CLIENT_APP_ID", NULL, CfgPath);//载入客户端APP ID
	//s32 FT_CLIENT_PORT = GetKeyValue("FT_CLIENT_PORT", NULL, CfgPath);//载入客户端端口

	s32 ret = GetKeyValue("STORGE_PATH", m_achLclStorgePath, CfgPath);	//载入下载文件本地存储位置

	if(m_bConn == FALSE)
	{
		m_dwServerNode = OspConnectTcpNode(inet_addr(Server_Ip), FT_SERVER_PORT, DisCon_Detection_Timeout, DisConn_Detction_Num, 0, NULL);
	
	//de_bug
		//printf("m_dwServerNode = %d \n",m_dwServerNode);
	
		if (m_dwServerNode == INVALID_NODE)
		{
			::OspPrintf(TRUE, FALSE, "Failed to Connect to the Server !\n");
			return ;
		}
	//设置结点断链检测函数
		if (!::OspSetHBParam(m_dwServerNode, DisCon_Detection_Timeout, DisConn_Detction_Num))
		{
			::OspPrintf(TRUE, FALSE, "Error,failed to Set OspSetHBParam!\n");
		}
	//设置结点断链通知函数
		if (::OspNodeDiscCBRegQ(m_dwServerNode, GetAppID(), FT_CLIENT_INS_NUM) != OSP_OK)
		{
			::OspPrintf(TRUE, FALSE, "Error,failed to Set OspNodeDiscCBRegQ!\n");
		}
	//向服务器发送请求
		::OspPost(MAKEIID(OSP_SERVER_APP_ID, CInstance::PENDING), FT_EVENT_S_PRCSS_REQ, NULL, 0, m_dwServerNode,MAKEIID(GetAppID(), GetInsID()));
		//de_bug
		//printf("A cmd has send to Server\n");
	}
	
}

//客户端处理服务器回复 函数
void FTcInstance::ClientProcessReply(CMessage *const pMsg)
{
	switch (pMsg->event)
	{
		case FT_EVENT_C_PRCSS_RPLY_C2S:
		{
			DealConnReply(pMsg);
			break;
		}
		case FT_EVENT_C_INQUIRE_ACK_RCV:
		{
			DealQueryFileReply(pMsg);
			break;
		}
		case FT_EVENT_C_PRCSS_RPLY_URL:
		{
			DealReqUrlRply(pMsg);
			break;
		}			
	default:
		break;
	}
	
}

//客户端请求下载文件列表函数
void FTcInstance::ClientQueryFile(CMessage *const pMsg)
{

	if (m_bConn == FALSE)
	{
		printf("IS NOT connected\n");
		return;
	}
	//de_bug
		//printf("ClientQueryFile() was called\n");
	TMsg TQuery;
	memset(&TQuery, 0, sizeof(TQuery));
	memcpy(&TQuery, (TMsg*)pMsg->content, sizeof(TQuery)); //获取用户指令，分为带参和不带参两种
	//de_bug
		//printf("Flag is %d \nMassage is %s \n",TQuery.m_bFlag,TQuery.m_achMsgBody);
	printf("\n%s\n",TQuery.m_achMsgBody);	
	//发送查询消息
	::OspPost(m_dwSevInstancId, FT_EVENT_S_FILE_LIST,&TQuery, sizeof(TQuery), m_dwServerNode, MAKEIID(GetAppID(), GetInsID()));
	
}



//客户端请求获得文件URL函数

void FTcInstance::ClientReqURL(CMessage *const pMsg)
{
	if (m_bConn == FALSE)
	{
		return;
	}
	s8 ReqUrl[FILEPATH_ABS_LEN];
	strcpy(ReqUrl,(s8*)pMsg->content); 
	//de_bug
		//printf("ReqUrl is %s \n",ReqUrl);
	::OspPost(m_dwSevInstancId, FT_EVENT_S_PRCSS_REQ_DOWNLOAD, ReqUrl, sizeof(ReqUrl), m_dwServerNode, MAKEIID(GetAppID(), GetInsID()));
}

//客户端处理服务器返回的关于URL信息的函数

void FTcInstance::ClientDownload(CMessage *const pMsg)
{
	//printf("ClientDownload was called\n");
	s8 fullpath[FILEPATH_ABS_LEN];//要下载文件本地完整路径
	GetFileNameFromUrl((s8*)pMsg->content, fullpath);// 从URL获取下载文件名称并得到本地路径
	//de_bug
		//printf("Fullpath is %s \n",fullpath);
	s64 OffSet = IsFileExist(fullpath);//用于向http服务器发送断点传送信息（在http头部）
	//de_bug
		//printf("OffSet is %d \n",OffSet);
	u16 flag;
	flag = CurlDownload((s8*)pMsg->content, fullpath, OffSet);
	if (flag != 0)
	{
		::OspPrintf(TRUE, FALSE, "文件下载失败！\n");
		//de_bug
		printf("File download failed\n");
	}
	else
	{
		::OspPrintf(TRUE, FALSE, "文件下载成功，路径为：%s\n",fullpath);
		//de_bug
		printf("[asynchronous message]  : A file download success,Storage path is: %s\n",fullpath);
	}	
}


/*====================================================================
模块名：	与服务器断开连接
返回值：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/
void FTcInstance::ClientDisconn(CMessage *const pMsg)
{
	//向服务器发送断开连接事件
	printf("Disconnecting\n");
	//::OspPost(m_dwSevInstancId, OSP_DISCONNECT, NULL, 0, m_dwServerNode, MAKEIID(GetAppID(), GetInsID()));
	::OspPrintf(TRUE, FALSE, "与服务器断开连接!\n");
}

void FTcInstance::DealConnReply(CMessage *const pMsg)
{
	TMsg TProcSevReply;
	memset(&TProcSevReply, 0, sizeof(TProcSevReply));
	memcpy(&TProcSevReply, (TMsg*)pMsg->content, sizeof(TProcSevReply));
	if (TProcSevReply.m_bFlag == FALSE)
	{
		//de_bug
		printf("server feject connect,%s, %d \n", TProcSevReply.m_achMsgBody, TProcSevReply.m_bFlag);

		::OspPrintf(TRUE, FALSE, "%s\n", TProcSevReply.m_achMsgBody);  //打印拒绝连接消息
		::OspPost(MAKEIID(GetAppID(), GetInsID()), OSP_DISCONNECT, NULL, 0); //断开osp
	}
	else
	{
		//de_bug
		//printf("server  connected, %s, %d \n",TProcSevReply.m_achMsgBody,TProcSevReply.m_bFlag);
		printf("server  connected!\n");
		m_dwSevInstancId = pMsg->srcid;  //保存服务器实例ID
		m_bConn = TRUE;			//更改服务器连接状态
	}
}
void FTcInstance::DealQueryFileReply(CMessage *const pMsg)
{
	TMsg* TQueryReply;

	TQueryReply = (TMsg*)pMsg->content;
	//memset(TQueryReply, 0, sizeof(TMsg));
	//memcpy(&TQuery, (TMsg*)pMsg->content, sizeof(TQuery)); //获取用户指令，分为带参和不带参两种
	printf("%s\n", TQueryReply->m_achMsgBody);
	::OspPrintf(TRUE, FALSE, "%s\n", TQueryReply->m_achMsgBody);  //打印服务器给出的文件列表或错误消息
}
void FTcInstance::DealReqUrlRply(CMessage *const pMsg)
{
	TMsg TUrlRcvd;
	memset(&TUrlRcvd, 0, sizeof(TUrlRcvd));
	memcpy(&TUrlRcvd, (TMsg*)pMsg->content, sizeof(TUrlRcvd));
	//如果请求的文件不存在或者请求的文件是一个文件夹，返回相应的错误信息或者该文件夹下的文件列表
	if (TUrlRcvd.m_bFlag == FALSE)
	{
		printf("\nYour req maybe incorrect \n");
		printf("%s\n", TUrlRcvd.m_achMsgBody);
		::OspPrintf(TRUE, FALSE, "%s\n", TUrlRcvd.m_achMsgBody);  //打印服务器给出的文件列表或错误消息
	}
	//获得请求文件的URL
	else
	{
		s8 Url[4000];
		memset(Url, 0, sizeof(Url));
		strcpy(Url, (s8*)TUrlRcvd.m_achMsgBody);
		//de_bug
		//printf("Url = %s\n",Url);
		//获得文件URL，调用下载函数
		::OspPost(MAKEIID(GetAppID(), GetInsID()), FT_EVENT_C_DOWNLOAD, Url, sizeof(Url));
	}
}

/*====================================================================
模块名：	Liburl下载函数（仅支持http以get方式下载）
返回值：	
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/

/*  libcurl write 回调函数 */
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}
s32 FTcInstance::CurlDownload(const s8* url, const u8* md5) {
	//首先检查下载文件夹是否存在,若不存在，创建此文件夹
	if(CheckDirExist(m_achLclStorgePath) == FALSE)
	{
		printf("Fail to create the download storage file\n");
		::OspPrintf(TRUE, FALSE, "Fail to create the download storage file\n\n");
		return -1;
	}
	s8 achFileName[FILENAME_MAX_LEN];
	if (FALSE == GetFileNameFromUrl(url, achFileName))
	{
		printf("[FTcInstance::CurlDownload] Fail to get the file name \n");
		::OspPrintf(TRUE, FALSE, "[FTcInstance::CurlDownload]Fail to get the file name\n");
		return -1;
	}
	s64 OffSet;
	//校验MD5值，并获取正确的文件偏移量
	if (FALSE == FileMd5Check(m_achLclStorgePath, md5, achFileName, &OffSet))
	{
		printf("[FTcInstance::CurlDownload] Fail to Check MD5 \n");
		::OspPrintf(TRUE, FALSE, "[FTcInstance::CurlDownload]Fail to Check MD5\n");
		return -1;
	}
	//获取写入文件本地绝对路径
	s8 achFileAbsPath[FILEPATH_ABS_LEN];
	if(NULL == GetFullPath(m_achLclStorgePath, achFileName, achFileAbsPath))
	CURL *curl;
	FILE *fp;
	CURLcode res;
	/*   调用curl_global_init()初始化libcurl  */
	res = curl_global_init(CURL_GLOBAL_ALL);
	if (CURLE_OK != res)
	{
		printf("init libcurl failed.");
		curl_global_cleanup();
		return -1;
	}
	/*  调用curl_easy_init()函数得到 easy interface型指针  */
	curl = curl_easy_init();
	if (curl) {
		if (OffSet == 0)
		{
			fp = fopen(achFileAbsPath, "wb");
			if (fp == NULL)
				perror("open the exist file failed!\n");
		}
		else
		{
			fp = fopen(achFileAbsPath, "ab");
			if (fp == NULL)
				perror("open file failed!\n");
		}
		/*  调用curl_easy_setopt()设置传输选项 */
		
		res = curl_easy_setopt(curl, CURLOPT_URL, url);

		if (res != CURLE_OK)
		{
			fclose(fp);
			curl_easy_cleanup(curl);
			return -1;
		}
		/*  根据curl_easy_setopt()设置的传输选项，实现回调函数以完成用户特定任务  */
		res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		if (res != CURLE_OK)
		{
			fclose(fp);
			curl_easy_cleanup(curl);
			return -1;
		}
		/*  根据curl_easy_setopt()设置的传输选项，实现回调函数以完成用户特定任务  */
		res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		if (res != CURLE_OK)
		{
			fclose(fp);
			curl_easy_cleanup(curl);
			return -1;
		}
		//如果本地已存在部分文件，则需要断点传续
		if (OffSet != 0)
		{
			s8 Range[30]; //存储断点下载范围
			sprintf(Range,"%ld",OffSet);
			strcat(Range,"-");
			curl_easy_setopt(curl, CURLOPT_RANGE, Range);
		}

		res = curl_easy_perform(curl);                               // 调用curl_easy_perform()函数完成传输任务  
		fclose(fp);
		/* Check for errors */
		if (res != CURLE_OK) {
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
			curl_easy_cleanup(curl);
			return -1;
		}

		/* always cleanup */
		curl_easy_cleanup(curl);                                     // 调用curl_easy_cleanup()释放内存   

	}
	curl_global_cleanup();
	return 0;
}



